var quotes = ["\"Husk blot på at der nogen derude, der er mere lykkelige, med mindre end hvad du har.\"", "\"Regel nr. 1 i livet: Gør hvad der gør DIG glad.\"", "\"Et opmuntrende ord under en fiasko er mere værd end én times ros efter en succes.\"", "\"Udfordringer gør livet interessant og at overkomme dem gør livet meningsfuldt.\"", "\"Du bliver hvad du tænker.\"", "\"Selvtillid er den mest attraktive kvalitet en person kan have. Hvordan skulle nogen kunne se, hvor fantastisk du er, hvis ikke du selv kan se det?\"", "\"Hvis du går gennem helvede, så bliv ved med at gå.\"", "\"Det eneste der står imellem dig og din drøm, er viljen til at forsøge og troen på, at det rent faktisk er muligt.\"", "\"Lykke kan ikke rejses til, ejes, tjenes eller bæres. Det er den spirituelle erfaring, der ligger i at leve hvert minut med kærlighed, ynde og taknemmelighed.\"", "\"Selvom ingen kan gå tilbage og lave en helt ny start, kan alle starte nu og lave en helt ny ende.\"", "\"En af nøglerne til succes er at gøre, hvad du ved du bør gøre, selvom du ikke har lyst til at gøre det.\"", "\"Tag ansvar for dit liv. Kun du selv kan bringe dig til dine mål, ingen andre.\"", "\"Fald syv gange, rejs dig otte.\"", "\"Din tid er begrænset, så spild den ikke på at leve en andens liv.\"", "\"Smerten du føler i dag, er styrken du vil føle i morgen. Hver udfordring du møder, er en mulighed for vækst.\""];

function start() {
   document.getElementById("centerdiv1").style.display = "none";
   document.getElementById("secondDiv").style.display = "inline";
   document.getElementById("video").play();
   document.getElementById("Audio").play();
   YEET();
}

var header = document.getElementById("cough");
header.addEventListener("animationiteration", YEET); 

document.getElementById("video").addEventListener('ended', (event) => {
   document.getElementById("secondDiv").style.display = "none";
   document.getElementById("lastDiv").style.display = "inline";
});

var stored = [];
function YEET() {
   var random = Math.floor(Math.random() * 14);
   if(!stored.includes(random)) {
      stored.push(random);
      document.getElementById("cough").innerHTML = quotes[random];
   } else YEET();
}

